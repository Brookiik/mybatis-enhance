package com.gitee.hengboy.mybatis.enhance.dsl.expression;

/**
 * 查询表达式接口
 *
 * @author：于起宇 <p>
 * ================================
 * Created with IDEA.
 * Date：2018/8/10
 * Time：2:40 PM
 * 简书：http://www.jianshu.com/u/092df3f77bca
 * 码云：https://gitee.com/hengboy
 * GitHub：https://github.com/hengyuboy
 * ================================
 * </p>
 */
public interface Expression<T> {
    /**
     * 根描述
     * 类型=com.gitee.hengboy.mybatis.enhance.dsl.serach.expression.ColumnExpression代表字段名称
     * 类型=com.gitee.hengboy.mybatis.enhance.dsl.serach.expression.TableExpression代表表名
     *
     * @return
     */
    String getRoot();

    /**
     * 添加别名，为字段 或者表
     *
     * @param asName 别名
     * @return 表达式
     */
    Expression<T> as(String asName);

    /**
     * 获取别名
     *
     * @return 别名
     */
    String getAsName();
}
