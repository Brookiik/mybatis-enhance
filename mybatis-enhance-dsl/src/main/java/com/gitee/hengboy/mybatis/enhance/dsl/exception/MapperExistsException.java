package com.gitee.hengboy.mybatis.enhance.dsl.exception;

import com.gitee.hengboy.mybatis.enhance.exception.EnhanceFrameworkException;

/**
 * mapper已经存在异常
 *
 * @author：于起宇 <p>
 * ================================
 * Created with IDEA.
 * Date：2018/8/10
 * Time：2:40 PM
 * 简书：http://www.jianshu.com/u/092df3f77bca
 * 码云：https://gitee.com/hengboy
 * GitHub：https://github.com/hengyuboy
 * ================================
 * </p>
 */
public class MapperExistsException
        extends EnhanceFrameworkException {

    public MapperExistsException(String message) {
        super(message);
    }
}
