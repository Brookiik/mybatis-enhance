package com.gitee.hengboy.mybatis.enhance.dsl.expression.interfaces;

import com.gitee.hengboy.mybatis.enhance.dsl.exception.ColumnException;
import com.gitee.hengboy.mybatis.enhance.dsl.expression.ColumnExpression;

/**
 * 数据库函数表达式
 * v1.0.0：count(_d_good.gPrice)
 * v1.0.3：_d_good.gPrice.count()
 *
 * @author：于起宇 <p>
 * ================================
 * Created with IDEA.
 * Date：2018/8/10
 * Time：2:40 PM
 * 简书：http://www.jianshu.com/u/092df3f77bca
 * 码云：https://gitee.com/hengboy
 * GitHub：https://github.com/hengyuboy
 * ================================
 * </p>
 */
public interface FunctionExpression<T>
        extends
        DateExpression<T> {

    /**
     * 设置返回的字段表达式
     *
     * @return 表达式
     * @throws ColumnException 列异常
     */
    ColumnExpression<T> getSlfe() throws ColumnException;

    /**
     * 查询某个字段的总数量
     *
     * @return 表达式
     * @throws ColumnException 列异常
     */
    ColumnExpression<T> count() throws ColumnException;

    /**
     * 查询某个字段的最大值
     *
     * @return 表达式
     * @throws ColumnException 列异常
     */
    ColumnExpression<T> max() throws ColumnException;

    /**
     * 查询某个字段的最小值
     *
     * @return 表达式
     * @throws ColumnException 列异常
     */
    ColumnExpression<T> min() throws ColumnException;

    /**
     * 查询某个字段的平均值
     *
     * @return 表达式
     * @throws ColumnException 列异常
     */
    ColumnExpression<T> avg() throws ColumnException;

    /**
     * 查询某个字段的总值
     *
     * @return 表达式
     * @throws ColumnException 列异常
     */
    ColumnExpression<T> sum() throws ColumnException;
}
